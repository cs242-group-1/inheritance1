package com.wsu.cs;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The test class StaffTest.
 *
 * @author  Karl R. Wurst
 *
 * @version 2020 - Revised S Nagpal
 * Included package and Updated Tests for JUnit5 - Jupiter
 *
 */

class StaffTest {

    /**
     * Default constructor for test class StaffTest
     */
    public StaffTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @org.junit.jupiter.api.BeforeAll
    static void setUp() {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @org.junit.jupiter.api.AfterAll
    static void tearDown() {
    }

    @org.junit.jupiter.api.Test
    public void testStaff() {
        Staff staff = new Staff("Fred", "Jones", "23456", "9876", 41000, 12);
        assertEquals("COM.WSU.CS.STAFF:\tFred Jones\tID = 23456\tExtension = 9876\tSalary = $41,000.00\tYears of Service = 12",
                staff.toString());
    }
}