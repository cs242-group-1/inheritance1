package com.wsu.cs;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author  Karl R. Wurst
 *
 * @version 2020 - Revised S Nagpal
 * Included package and Updated Tests for JUnit5 - Jupiter
 *
 */

class FacultyTest {

    /**
     * Default constructor for test class Lab10Test
     */
    public FacultyTest()
    {
    }

    @org.junit.jupiter.api.BeforeAll
    static void setUp() {
    }

    @org.junit.jupiter.api.AfterAll
    static void tearDown() {
    }

    @org.junit.jupiter.api.Test
    public void testFaculty1() {
        Faculty faculty1 = new Faculty("Mary", "Johnson", "34567", "5432", 52000, "Assistant Professor");
        assertEquals("COM.WSU.CS.FACULTY:\tMary Johnson\tID = 34567\tExtension = 5432\tSalary = $52,000.00\tFaculty Rank = Assistant Professor",
                faculty1.toString());
    }

    @org.junit.jupiter.api.Test
    public void testFaculty2() {
        Faculty faculty2 = new Faculty("Peter", "Clinton", "34987", "1290", 64000, "Professor");
        assertEquals("COM.WSU.CS.FACULTY:\tPeter Clinton\tID = 34987\tExtension = 1290\tSalary = $64,000.00\tFaculty Rank = Professor",
                faculty2.toString());
    }
}