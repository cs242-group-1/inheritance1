package com.wsu.cs;

public class Employee extends Person {
    private String extension;
    private double salary;

    Employee(String firstName, String lastName, String id, String extension, double salary) {
        super(firstName, lastName, id);
        this.extension = extension;
        this.salary = salary;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
